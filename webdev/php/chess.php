<?php
class Pieces{}

class King extends Pieces{
   function __construct($pos, $symb){
      $this->symbol = $symb;
      $this->pos = $pos;
   }

   function calculate_moves($GEN = 1){
      $moves = array();
      for ($x=-1; $x < 2; $x++){
         for ($y=-1; $y < 2; $y++){
            $move = array($this->pos[0]+1*$x, $this->pos[1]+1*$y);
            if ($move[0] > 0 and $move[0] < 9 and $move[1] > 0 and $move[1] < 9){
               array_push($moves, $move);
            }
         }
      }
      return $moves;
   }
}

class Queen extends Pieces{
   function __construct($pos, $symb){
      $this->symbol = $symb;
      $this->pos = $pos;
   }

   function calculate_moves($GEN = 1){
      $moves = array();
      for ($x = $this->pos[0], $y = $this->pos[1]; $x < 9, $y < 9; $x++, $y++){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $x > 0, $y > 0; $x--, $y--){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $x < 9, $y > 0; $x++, $y--){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $x > 0, $y < 9; $x--, $y++){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $x < 9; $x++){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $y < 9; $y++){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $x > 0; $x--){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $y > 0; $y--){ array_push($moves, array($x, $y)); }

      return $moves;
   }
}

class Pawn extends Pieces{
   function __construct($pos, $symb){
      $this->symbol = $symb;
      $this->pos = $pos;
   }

   function move($P, $GEN = 1){
      // direction gets decided via the players color
      if ($this->pos[0]+1 > 8) return array(array($this->pos[0]+1, $this->pos[1]), $this->pos);
   }
}

class Rook extends Pieces{
   function __construct($pos, $symb){
      $this->symbol = $symb;
      $this->pos = $pos;
   }

   function calculate_moves($GEN = 1){
      $moves = array();
      for ($x = $this->pos[0], $y = $this->pos[1]; $x < 9; $x++){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $y < 9; $y++){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $x > 0; $x--){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $y > 0; $y--){ array_push($moves, array($x, $y)); }

      return $moves;
   }
}

class Bishop extends Pieces{
   function __construct($pos, $symb){
      $this->symbol = $symb;
      $this ->pos = $pos;
   }

   function calculate_moves($GEN = 1){
      $moves = array();
      for ($x = $this->pos[0], $y = $this->pos[1]; $x < 9 and $y < 9; $x++, $y++){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $x < 9 and $y > 0; $x++, $y--){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $x > 0 and $y < 9; $x--, $y++){ array_push($moves, array($x, $y)); }
      for ($x = $this->pos[0], $y = $this->pos[1]; $x > 0 and $y > 0; $x--, $y--){ array_push($moves, array($x, $y)); }

      return $moves;
   }
}

class Knight extends Pieces{
   function __construct($pos, $symb){
      $this->symbol = $symb;
      $this->pos = $pos;
   }

   function calculate_moves($GEN = 1){
      $moves = array();
      //$move =
   }
}

class Player extends Pieces{
   function __construct($username, $wins, $color){
      $this->username = $username;
      $this->wins = 0;
      $this->color = $color;
      $this->my_pieces = $this->setup();
      $this->graveyard = array();
   }

   function setup(){
      $my_pieces = array();
      $x1 = 2; $x2 = 1; $symbols = array("♔", "♕", "♖", "♗", "♘", "♙");    // white player (x1 = front row, x2 = back row)
      if ($this->color == "black"){ $x1 = 7; $x2 = 8; $symbols = array("♚","♛", "♜", "♝", "♞", "♟"); }
      // pawns
      for ($y = 1; $y < 9; $y++){ array_push($my_pieces, new Pawn(array($x1, $y), $symbols[5])); }
      //rook
      array_push($my_pieces, new Rook(array($x2, 1), $symbols[2]));
      array_push($my_pieces, new Rook(array($x2, 8), $symbols[2]));
      //knights
      array_push($my_pieces, new Knight(array($x2, 2), $symbols[4]));
      array_push($my_pieces, new Knight(array($x2, 7), $symbols[4]));
      //bishops
      array_push($my_pieces, new Bishop(array($x2, 3), $symbols[3]));
      array_push($my_pieces, new Bishop(array($x2, 6), $symbols[3]));
      //queen & king
      array_push($my_pieces, new Queen(array($x2, 5), $symbols[1]));
      array_push($my_pieces, new King(array($x2, 4), $symbols[0]));

      return $my_pieces;
   }

   function remove_piece($Piece){
      // TODO:
      array_push($this->graveyard, $Piece);
   }

   function revive_piece(){
      for ($i = 0; $i < sizeof($this->graveyard); $i++){
         echo "{$i}: {$this->graveyard[i]->symbol}";
      }
      echo "select the piece which you want to revive.\nchoice: "; $choice = fgets(STDIN);
      array_push($this->my_pieces, $this->graveyard[$choice]);
      unset($this->graveyard[$choice]);
      $this->graveyard = array_values($this->graveyard);
   }
}

class ChessBoard extends Player{
   function __construct($players){
      $this->players = $players;
      $this->over = False;
      $this->field = $this->generate_board();
   }

   function get_piece($koord, $S){
      foreach ($this->players as $player){
         foreach ($player->my_pieces as $piece){
            if ($piece->pos == $koord){
               if ($S) return $piece->symbol;
               return $piece;
            }
         }
      }
      return "_";
   }

   function generate_board(){
      $board = array();   // board is an array of rows
      for ($x = 8; $x > 0; $x--){
         $row = "{$x}. ";
         for ($y = 1; $y < 9; $y++){
            $row = "{$row}|_{$this->get_piece(array($x, $y), True)}_";
         }
         array_push($board, $row);
      }
      $this->update($board);
   }

   function update($board){
      system('clear'); echo "    A.   B.  C.  D.  E.  F.  G.  H.\n";
      foreach ($board as $row){ echo "{$row}|\n"; }
   }

   function move($P, $sel, $dest){
      $piece = $this->get_piece($sel, False);
      $piece->pos = $dest;
      $this->generate_board();
   }

   function game(){
      for ($i = 0; !$this->over; $i++){
         $curr_player = $this->players[$i%2];
         $EX = Null;
         // wait until the player enters a valid command.
         while(!preg_match("/\b(MV|mv)[1-8]([A-H]|[a-h])(TO|to)[1-8]([A-H]|[a-h])/", $EX)){
            echo "INPUT: "; $EX = preg_replace('/\s\s+/', '', fgets(STDIN));
         }
         //preg_match("@^(?:MV)?([^[A-H]+)@i", strtoupper($EX), $F); $f_x = $F[sizeof($F)-1];
         $sel = array((int)$EX[2], ord(strtoupper($EX[3])) % 64);
         $dest = array((int)$EX[6], ord(strtoupper($EX[7])) % 64);
         $this->move($curr_player, $sel, $dest);
      }
   }
}

$NILS = new Player("Nils", 0, "black");
$JORDAN = new Player("Jordan", 0, "white");

$CB = new ChessBoard(array($NILS, $JORDAN));
$CB->game();
?>
