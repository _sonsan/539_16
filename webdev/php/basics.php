<?php

// Caluclate $n!
function faculty($n){
   if ($n > 0){
      return ($n  *= faculty($n - 1));
   }
   else{
      return 1;
   }
}

// Raise $b to the power of $e
function power($b, $e){
   $tmp = $b;
   for ($i = 1; $i < $e; $i++){
      $b *= $tmp;
   }
   return $b;
}

// Check if $num is a happy number.
function is_happy($num){
   $tmp = 0;
   $str_num = (string)$num;
   for ($i = 0; $i < strlen($str_num); $i++){
      $tmp += (int)$str_num[$i]**2;
   }
   switch ($tmp){
      case 1:
         return "True";
         break;
      case 4:
         return "False";
         break;
      default:
         return is_happy($tmp);
   }
}

function test($fn, $ln, $age){
   echo "$fn $ln is $age years old.\n";

}

// cli input.
$n = (int)fgets(STDIN);


test('Nils', 'Sterz', '20');
echo "5! = ".faculty(5)."\n";
echo "2^5 = ".power(2, 5)."\n";
echo is_happy($n)."\n";

?>
